import React from 'react';
import { Dimensions, StyleSheet, View } from 'react-native';
import {  } from 'react-native-paper';

const height = Dimensions.get('window').height

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})

const LoginScreen = (props) => {
    return (
        <View style={Styles.container} >
            <Text>Login Here</Text>
        </View>
    )
}

export default LoginScreen;