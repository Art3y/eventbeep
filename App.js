import React from 'react';
import { StyleSheet, View, Image, YellowBox } from 'react-native';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { Constants, Font, AppLoading, Asset } from 'expo';
import * as firebase from 'firebase';

import Apikeys from './Firebase/Original';
// import Original from './Firebase/Original';
import AppNavigator from './src/Navigations/AppNavigator';
import RootNavigator from './src/Navigations/RootNavigator';
import NavigationService from './src/Navigations/NavigationService';

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    statusBar: {
        backgroundColor: '#fff',
        height: Constants.statusBarHeight,
    },
    loadingContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    loadingLogo: {
        
    },
});

const theme = {
    ...DefaultTheme,
    roundness: 2,
    colors: {
        ...DefaultTheme.colors,
        primary: '#3498db',
        accent: '#f1c40f',
    }
};

YellowBox.ignoreWarnings(['Setting a timer'])

export default class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isAuthenticationReady: false,
            isAuthenticated: false,
            fontLoaded: false,
        }
        if(!firebase.apps.length) {
            firebase.initializeApp(Apikeys.FirebaseConfig)
        };
        firebase.firestore().settings({timestampsInSnapshots: true})
        firebase.auth().onAuthStateChanged(this.onAuthStateChanged);
    }

    async _cacheResourceAsync() {
        const images = [
            require('./src/Assets/Images/loading.gif')
        ];
      
        const cacheImages = images.map((image) => {
            return Asset.fromModule(image).downloadAsync();
        });
        
        return Promise.all(cacheImages)
    }

    async componentDidMount() {
        await Font.loadAsync({
            'Helvetica': require('./assets/fonts/Helvetica/Helvetica-Regular.ttf'),
            'Karla': require('./assets/fonts/karla/Karla-Regular.ttf'),
        });
        this.setState({ fontLoaded: true });
    }

    componentDidMount() {
		this.setState({loadingLogo: require('./src/Assets/Images/loading.gif')})
        Font.loadAsync({
            'Helvetica': require('./assets/fonts/Helvetica/Helvetica-Regular.ttf'),
            'Karla': require('./assets/fonts/karla/Karla-Regular.ttf'),
        });
    }

    onAuthStateChanged = (user) => {
        this.setState({isAuthenticationReady: true});
        this.setState({isAuthenticated: !!user});
    }

    render() {
        if(!this.state.isAuthenticationReady && !this.state.fontLoaded) {
            return(
                <View style={Styles.loadingContainer} >
                    <View style={Styles.statusBar} />
                    <Image resizeMode='contain' style={Styles.loadingLogo} source={this.state.loadingLogo} />
                </View>
            )
        }
        else {
            return (
                <PaperProvider theme={theme}>
                    <View style={Styles.container}>
                        <View style={Styles.statusBar} />
                        <View style={{flex: 12}} >
                            {
                                (this.state.isAuthenticated) ? 
                                <RootNavigator 
                                    ref={navigatorRef => {
                                        NavigationService.setTopLevelNavigator(navigatorRef);
                                    }}
                                    navigation={this.props.navigation} /> 
                                : 
                                <AppNavigator navigation={this.props.navigation} />
                            }
                        </View>
                    </View>
                </PaperProvider>
            );
        }
    }
}
