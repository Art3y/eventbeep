const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

exports.firestoreUserEntry = functions.https.onCall((dat, context) => {
    // const data = JSON.parse(dat)
    console.log(dat)
    admin.firestore().collection('users').doc(dat.docId).set(dat.info)
        .then((res) => {
            console.log(res)
        })
        .catch((err) => {
            console.log(err)
        })
    // console.log('hello vishnu')
})

// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//     response.send('Hello from Vishnu')
// });
