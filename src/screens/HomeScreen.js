import React, { Component } from 'react';
import { AppLoading, Asset } from 'expo';
import { View, StyleSheet, Dimensions } from 'react-native';
import { Tab, Tabs, ScrollableTab } from 'native-base';
import { Modal } from 'react-native-paper';

import HomeHeader from '../components/Header/HomeHeader';
import Search from '../components/Header/Search';
import HomeTab from '../components/Tabs/Home';
import AdventuresTab from '../components/Tabs/Adventures';
import CollegeEventsTab from '../components/Tabs/CollegeEvents';
import FoodTab from '../components/Tabs/Food';
import GamingTab from '../components/Tabs/Gaming';
import MusicDanceTab from '../components/Tabs/MusicDance';
import NightlifeTab from '../components/Tabs/Nightlife';
import SportsTab from '../components/Tabs/Sports';
import WorkSemiTab from '../components/Tabs/WorkSemi';

const height = Dimensions.get('window').height

const Styles = StyleSheet.create({
    wrap: {
        flex: 1,
        backgroundColor: '#e0e0e0',
    },
    tabContainer: {
        // height: 60
        // color: '#fff'
    },
    tab: {
        // height: 60
    },
    tabText: {
        fontFamily: 'Karla',
    },
    activeTabText: {
        fontFamily: 'Karla',
        // color: 'red'
    },
    underline: {
        height: 1
    }
})

class HomeScreen extends Component {

    constructor(props) {
		super(props)
		this.state = {
			ready: false,
			loading: false,
			headerLogo: null,
            modalVisible: false,
            modalType: 0,
            page: 0
        }
	}

	async _cacheResourceAsync() {
        const images = [
            require('../Assets/Images/logo.png')
        ];
      
        const cacheImages = images.map((image) => {
            return Asset.fromModule(image).downloadAsync();
        });
        
        return Promise.all(cacheImages)
    }

    openModalHandler = (type) => {
        this.setState({modalVisible: true, modalType: type})
    }

    closeModalHandler = () => {
        this.setState({modalVisible: false})
    }

    pageHandler = (page) => {
        this.setState({page: page})
    }

    render() {
        if(!this.state.ready) {
            return (
                <AppLoading
                    startAsync={this._cacheResourceAsync}
                    onFinish={() => this.setState({ready: true})}
                    onError={console.warn} />
            )
        }
        
        return (
            <View style={Styles.wrap} >
                <HomeHeader
                    headerLogo={require('../Assets/Images/logo.png')}
                    openModal={() => this.openModalHandler(1)} />
                <View style={{flex: 1}} >
                    <Tabs initialPage={this.state.page} page={this.state.page} tabBarUnderlineStyle={Styles.underline} renderTabBar={()=> <ScrollableTab />} >
                        <Tab textStyle={Styles.tab} activeTextStyle={Styles.activeTabText} tabStyle={Styles.tab} heading='Home' >
                            <HomeTab navigation={this.props.navigation} pageHandler={this.pageHandler} />
                        </Tab>
                        <Tab textStyle={Styles.tab} activeTextStyle={Styles.activeTabText} tabStyle={Styles.tab} heading='Adventure' >
                            <AdventuresTab navigation={this.props.navigation} />
                        </Tab>
                        <Tab textStyle={Styles.tab} activeTextStyle={Styles.activeTabText} tabStyle={Styles.tab} heading='College Events' >
                            <CollegeEventsTab navigation={this.props.navigation} />
                        </Tab>
                        <Tab textStyle={Styles.tab} activeTextStyle={Styles.activeTabText} tabStyle={Styles.tab} heading='Food' >
                            <FoodTab navigation={this.props.navigation} />
                        </Tab>
                        <Tab textStyle={Styles.tab} activeTextStyle={Styles.activeTabText} tabStyle={Styles.tab} heading='Gaming' >
                            <GamingTab navigation={this.props.navigation} />
                        </Tab>
                        <Tab textStyle={Styles.tab} activeTextStyle={Styles.activeTabText} tabStyle={Styles.tab} heading='Music and Dance' >
                            <MusicDanceTab navigation={this.props.navigation} />
                        </Tab>
                        <Tab textStyle={Styles.tab} activeTextStyle={Styles.activeTabText} tabStyle={Styles.tab} heading='Nightlife' >
                            <NightlifeTab navigation={this.props.navigation} />
                        </Tab>
                        <Tab textStyle={Styles.tab} activeTextStyle={Styles.activeTabText} tabStyle={Styles.tab} heading='Sports' >
                            <SportsTab navigation={this.props.navigation} />
                        </Tab>
                        <Tab textStyle={Styles.tab} activeTextStyle={Styles.activeTabText} tabStyle={Styles.tab} heading='Workshop and Seminars' >
                            <WorkSemiTab navigation={this.props.navigation} />
                        </Tab>
                    </Tabs>
                </View>
                <Modal
                    animationType="slide"
                    visible={this.state.modalVisible} 
                    onRequestClose={this.closeModalHandler} >
                    <Search 
                        closeModal={this.closeModalHandler}
                        disabled={this.state.modalType !== 1} />
                </Modal>
            </View>
        );
    };
};

export default HomeScreen;