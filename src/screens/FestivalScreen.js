import React, { Component } from 'react';
import { AppLoading, Asset } from 'expo';
import { View, Text, StyleSheet, Dimensions, Image, FlatList } from 'react-native';
import HomeHeader from '../components/Header/HomeHeader';
import firebase from 'firebase';
import FestivalCard from '../components/Festivals/FestivalCard';
require('firebase/firestore');

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

const Styles = StyleSheet.create({
    wrap: {
		flex: 1,
		backgroundColor: '#e0e0e0',
	},
	loadingWrap: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#e0e0e0'
    },
    loadingImage: {
        height: height * 0.5,
        width: width * 0.7
    },
    loadingText: {
        marginTop: 10,
        fontFamily: 'Helvetica',
        fontSize: 24,
        fontWeight: '600',
        color: '#757575'
    }

})

class FestivalScreen extends Component {

	constructor(props) {
		super(props)
		this.state = {
			ready: false,
			loading: true,
			headerLogo: null,
			festivals: []
		}
		this.fetchFestivals()
	}

	async _cacheResourceAsync() {
        const images = [
			require('../Assets/Images/logo.png'),
            require('../Assets/Images/no_events.png')
        ];
		
        const cacheImages = images.map((image) => {
            return Asset.fromModule(image).downloadAsync();
        });
        
        return Promise.all(cacheImages)
    }

	componentDidMount() {

	}

	fetchFestivals = () => {
		firebase.firestore().collection('festivals').where('expired', '==', false).get()
			.then((snapshot) => {
				snapshot.forEach((doc) => {
					this.setState({festivals: [...this.state.festivals, {id: doc.id, fest: this.generateCardData(doc.data())}]})
				})
				this.setState({loading: false})
			})
			.catch((err) => {
				console.log('Festivals fetch error', err)
			})
	}

	generateCardData = (doc) => {
		let cardData = {}
		cardData.categories = doc.categories
        cardData.imagePrimary = doc.imagePrimary
        if(doc.name.length > 36) {
            cardData.name = doc.name.slice(0, 36).concat("", "...")
        }
        else {
            cardData.name = doc.name
        }
        let dateStart = new Date(doc.timeStart)
        let dateEnd = new Date(doc.timeEnd)		
		cardData.dateStart = dateStart.getDate()
		cardData.dateEnd = dateEnd.getDate()
		cardData.monthStart = this.setMonth(dateStart.getMonth())
		cardData.monthEnd = this.setMonth(dateEnd.getMonth())
        cardData.yearStart = dateStart.getFullYear()
        cardData.yearEnd = dateEnd.getFullYear()
		
		cardData.period = String(cardData.monthStart).concat(',', String(cardData.dateStart)).concat('-', String(cardData.monthEnd)).concat(',', String(cardData.dateEnd)).concat(' ', String(cardData.yearEnd))

		return cardData
	}
	
	setMonth = (date) => {
		let month = 'JAN'
		switch(date) {
            case(0): {month = 'JAN'; break};
            case(1): {month = 'FEB'; break};
            case(2): {month = 'MAR'; break};
            case(3): {month = 'APR'; break};
            case(4): {month = 'MAY'; break};
            case(5): {month = 'JUN'; break};
            case(6): {month = 'JUL'; break};
            case(7): {month = 'AUG'; break};
            case(8): {month = 'SEP'; break};
            case(9): {month = 'OCT'; break};
            case(10): {month = 'NOV'; break};
            case(11): {month = 'DEC'; break};           
		}
		return month
	}

	openModalHandler = (festID) => {
		const fest = this.state.festivals.findIndex(item => item.id == festID)
		this.setState({expandedFest: this.state.festivals.findIndex(item => item.id == festID)})
        this.props.navigation.navigate('FestivalHome', {fest: this.state.festivals[fest], closeModal: () => this.props.navigation.goBack()})
    }

	_renderItem = ({item}) => {
        return (
            <FestivalCard
                name={item.fest.name}
                period={item.fest.period}
                image={item.fest.imagePrimary}
                openModal={() => this.openModalHandler(item.id)} />
        )
    }

    _keyExtractor = (item, index) => item.id;

    render() {

		if(this.state.loading) {
            return (
                <View style={Styles.loadingWrap} >
					<HomeHeader headerLogo={require('../Assets/Images/logo.png')} />
                    <Image style={Styles.loadingImage} resizeMethod='auto' source={require('../Assets/Images/no_events.png')} />
                    <Text style={Styles.loadingText} > Sorry! No festivals here</Text>
					<AppLoading
						startAsync={this._cacheResourceAsync}
						onFinish={() => this.setState({ready: true})}
						onError={console.warn} />
                </View>
            )
        }

		else {
			return(
				<View style={Styles.wrap} >
					<HomeHeader headerLogo={require('../Assets/Images/logo.png')} />
					<FlatList
						data={this.state.festivals}
						keyExtractor={this._keyExtractor}
						renderItem={this._renderItem}
						onEndReachedThreshold={0.5} />
				</View>
			);
		}
	}
}

export default FestivalScreen;
