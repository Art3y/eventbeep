import React, { Component } from 'react';
import { AppLoading, Asset } from 'expo';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import HomeHeader from '../components/Header/HomeHeader';

const height = Dimensions.get('window').height

const Styles = StyleSheet.create({
    wrap: {
		flex: 1,
        backgroundColor: '#e0e0e0',
    }
})

class TimelineScreen extends Component {

	constructor(props) {
		super(props)
		this.state = {
			ready: false,
			loading: false,
			headerLogo: null,
		}
	}

	async _cacheResourceAsync() {
        const images = [
            require('../Assets/Images/logo.png')
        ];
      
        const cacheImages = images.map((image) => {
            return Asset.fromModule(image).downloadAsync();
        });
        
        return Promise.all(cacheImages)
    }

	componentDidMount() {
		this.setState({headerLogo: require('../Assets/Images/logo.png')})
	}

    render() {

		if(!this.state.ready) {
            return (
                <AppLoading
                    startAsync={this._cacheResourceAsync}
                    onFinish={() => this.setState({ready: true})}
                    onError={console.warn} />
            )
        }		

      	return(
        	<View style={Styles.wrap} >
				<HomeHeader headerLogo={this.state.headerLogo} />
				<Text>Festivals</Text>
			</View>
    	);
  	}
}

export default TimelineScreen;
