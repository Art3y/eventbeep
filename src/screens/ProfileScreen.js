import React, { Component } from 'react'
import { AppLoading, Asset } from 'expo';
import { StyleSheet, Modal, ScrollView, Dimensions, View, Text, TouchableWithoutFeedback, Image } from 'react-native';
import { Button } from 'react-native-paper';
import firebase from 'firebase';
require('firebase/firestore');

import Details from '../components/Profile/Details';
import EditDetails from '../components/Profile/EditDetails';
import Interests from '../components/Profile/Interests';
import Refferal from '../components/Profile/Refferal';
import Buffer from '../components/Profile/Buffer'
import ResetPassword from '../components/Profile/ResetPassword';
import AddInterests from '../components/Profile/AddInterests';
import { Picker } from 'native-base';

const height = Dimensions.get('window').height

const Styles = StyleSheet.create({
    wrap: {
        flex: 1,
        padding: 10,
        backgroundColor: '#e0e0e0',
    },
    btn: {
        marginTop: 20,
        backgroundColor: 'yellow'
    },
    terms: {
        marginTop: 20,
        height: height * 0.08,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    loadingContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'green'
    },
    label: {
        backgroundColor: 'yellow',
        margin: 8,
        padding: 4
    },
})

class ProfileScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            modalType: 0,
            loading: true,
            info: {
                name: '',
                phone: '',
                email: '',
                collegespace: ''
            },
            notReady: false,
            headerLogo: null,
            interests: [],
            name: '',
            dob: '',
            branch:'',
            yop: '',
            degree: '',
            whatsapp: '',
            email: ''
        }
        this.dateHandler = this.dateHandler.bind(this);
    }

    async _cacheResourceAsync() {
        const images = [
            require('../Assets/Images/EeeBee.png'),
            require('../Assets/Images/loading.gif')
        ];
      
        const cacheImages = images.map((image) => {
            return Asset.fromModule(image).downloadAsync();
        });
        
        return Promise.all(cacheImages)
    }

    componentDidMount() {
        this.setState({loadingLogo: require('../Assets/Images/loading.gif')})
        this.setState({headerLogo: require('../Assets/Images/EeeBee.png')})
        this.fetchProfile();
        this.fetchInterests();
        this.fetchPickerData();
        this.setState({notReady: false})
    }

    fetchProfile = () => {
        firebase.firestore().collection('users').doc(firebase.auth().currentUser.email).get()
            .then((doc) => {
                this.setState({info: doc.data()})
                this.setState({
                    name: this.state.info.name, 
                    branch: this.state.info.branch,
                    degree: this.state.info.degree,
                    dob: this.state.info.dob,
                    email: this.state.info.email,
                    yop: String(this.state.info.yop),
                    whatsapp: this.state.info.whatsapp,
                    phone: String(this.state.info.phone),
                    college: this.state.info.collegespace
                })
            })
            .catch((err) => {
                console.log('ProfileScreen Error occured', err)
            })
    }

    fetchInterests = () => {
        firebase.firestore().collection('users').doc(firebase.auth().currentUser.email).collection('interests').get()
            .then((snapshot) => {
                snapshot.forEach((doc) => {
                    this.setState({interests: [...this.state.interests, doc.data()]})
                })
                this.setState({loading: false})
            })
            .catch((err) => {
                console.log('Unable to fetch intersets of users', err)
            })
    }

    fetchPickerData = () => {
        firebase.firestore().collection('extras').doc('degree').get()
            .then((snap) => {
                console.log(snap.data().degree)
                this.setState({degreeList: snap.data().degree})
            })
            .catch((err) => {
                console.log(err)
            })
    }

    dateHandler = (newDate) => {
        this.setState({dob: newDate})
        console.log(this.state.dob)
    }

    _renderItem = ({item}) => {
        return (
            <Text style={Styles.label} > {item.name} </Text>
        )
    }

    _keyExtractor = (item, index) => item.name; 

    _renderDegree = ({item}) => {
        return (
            <Picker.Item label={item} value={item} />
        )
    }

    _keyExtractorDegree = (item, index) => item

    saveDetails = () => {
        const updatedInfo = {
            branch: this.state.branch,
            degree: this.state.degree,
            dob: this.state.dob,
            email: this.state.email,
            name: this.state.name,
            yop: Number(this.state.yop),
            whatsapp: this.state.whatsapp
        }
        console.log(updatedInfo)
        firebase.firestore().collection('users').doc(firebase.auth().currentUser.email).update(updatedInfo)
            .then((res) => {
                console.log('Successfully updated user details')
            })
            .catch((err) => {
                console.log('Unable to update User details', err)
            })
    }

    signOut = async() => {
        firebase.auth().signOut()
            .then((res) => {
                console.log('Success')
            })
            .catch((err) => {
                console.log(err)
            })
    }
 
    openModalHandler = (type) => {
        this.setState({modalVisible: true, modalType: type})
    }

    closeModalHandler = () => {
        this.setState({modalVisible: false})
    }

    handleChange = (name, value) => {
        this.setState({
            [name]: value,
        });
    };

    handleDateChange = (newDate) => {
        this.setState({dob: newDate})
    }

    degreeHandler = (value) => {
        this.setState({degree: value})
    }

    render() {

        if(this.state.notReady && this.state.loading) {
            return (
                <View style={Styles.loadingContainer} >
                    <AppLoading
                        startAsync={this._cacheResourceAsync}
                        onFinish={() => this.setState({ready: true})}
                        onError={console.warn} />
                    <View style={Styles.statusBar} />
                    <Image resizeMode='contain' source={this.state.loadingLogo} />
                </View>
            )
        }

        else {
            return (
                <ScrollView style={Styles.wrap} showsVerticalScrollIndicator={false} >
                    <Details
                        name={this.state.info.name}
                        email={this.state.info.email}
                        phone={this.state.info.phone}
                        college={this.state.info.collegespace}
                        headerLogo={this.state.headerLogo}
                        openModal={() => this.openModalHandler(1)} />
                    <Interests
                        interests={this.state.interests}
                        _renderItem={this._renderItem}
                        _keyExtractor={this._keyExtractor}
                        openModal={() => this.openModalHandler(2)} />
                    <Refferal />
                    <Buffer openModal={() => this.openModalHandler(3)} />
                    <Button style={Styles.btn} mode="contained" onPress={this.signOut} > SIGN OUT </Button>
                    <TouchableWithoutFeedback onPress={() => console.log('Terms')} >
                        <View style={Styles.terms} >
                            <Text style={{fontSize: 10, color: 'blue',}} >Terms and Privacy Policy</Text>
                            <Text style={{fontSize: 30, fontWeight: 'bold', color: 'blue'}} >EventBeep</Text>
                        </View>
                    </TouchableWithoutFeedback>
                    <View style={{height: height * 0.1}} ></View>
                    <Modal
                        animationType="slide"
                        visible={this.state.modalVisible} 
                        onRequestClose={this.closeModalHandler} >
                        <EditDetails 
                            handleChange={this.handleChange}
                            inputs={this.state}
                            setDate={this.dateHandler}
                            degreeHandler={this.degreeHandler.bind(this)}
                            date={new Date(this.state.dob)}
                            _renderItem={this._renderDegree}
                            _keyExtractor={this._keyExtractorDegree}
                            dateChange={this.handleDateChange}
                            headerLogo={this.state.headerLogo}
                            save={this.saveDetails}
                            closeModal={this.closeModalHandler}
                            disabled={this.state.modalType !== 1} />
                        <AddInterests
                            closeModal={this.closeModalHandler}
                            disabled={this.state.modalType !== 2} />
                        <ResetPassword
                            closeModal={this.closeModalHandler}
                            disabled={this.state.modalType !== 3} />
                    </Modal>
                </ScrollView>
            )
        }        
    }
}

export default ProfileScreen