import React from 'react';
import { View, ScrollView, Text, StyleSheet, TouchableOpacity, Dimensions, FlatList } from 'react-native';
import { FontAwesome, MaterialCommunityIcons, Ionicons } from '@expo/vector-icons';
import { Button } from 'react-native-paper';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import Modal from "react-native-modal";

import Venue from './Venue'
import EventDescription from './EventDescription';
import CarouselItem from './CarouselItem';
import EventHeader from '../Header/EventHeader';
import TicketCounter from './TicketCounter';

const height = Dimensions.get('window').height

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexGrow: 1,
        backgroundColor: '#ECF0F1',
    },
    carousel: {
        height: height * 0.3,
        paddingTop: 6,
        backgroundColor: 'transparent',
        width: '100%',
        position: 'relative'
    },
    pagination: {
        backgroundColor: 'transparent', 
        paddingVertical: 8,
        width: '100%',
        position: 'absolute',
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    summary: {
        width: '100%',
        height: height * 0.3,
        flexDirection: 'column',
        marginTop: 6,
        paddingTop: 4,
        paddingBottom: 4,
        backgroundColor: '#fff'
    },
    summaryView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-evenly',
        flexDirection: 'row'
    },
    summaryIcon: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    summaryText: {
        paddingEnd: 4,
        paddingStart: 4,
        flex: 6,
        fontWeight: '600',
        fontSize: 14,
        fontFamily: 'Karla'
    },
    contactConatiner: {
        height: height * 0.2,
        width: '100%',        
        flexDirection: 'column',
        padding: 6,
        marginTop: 6,
        backgroundColor: '#fff'
    },
    contactText: {
        fontSize: 20,
        fontFamily: 'Karla',
        fontWeight: 'bold'
    },
    tagsConatiner: {
        height: height * 0.15,
        width: '100%',
        marginTop: 6,
        padding: 6,
        backgroundColor: '#fff'
    },
    tagsText: {
        fontSize: 20,
        fontWeight: 'bold',
        fontFamily: 'Karla'
    },
    tags: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    tag: {
        margin: 2,
        backgroundColor: 'yellow',
        fontSize: 16,
        fontWeight: '400',
        fontFamily: 'Karla'
    },
    viewTickets: {
        backgroundColor: '#fff',
        margin: 8,
        marginTop: 0
    },
    modal: {
        justifyContent: 'flex-end',
        margin: 0
    }
});

class EventDetails extends React.Component {
    
    constructor(props){
        super(props);

        this.state = {
            title       : props.title,
            sliderWidth : Dimensions.get('window'),
            activeSlide : 0,
            datacarousel: [],
            expansion: true,
            loading: true,
            modalVisible: false,
            count: 1,
        };
    }

    componentDidMount() {
        this.generateDataCarousel()
        this.accordionHandler()
        this.setState({
            timeStart: this.getDates(this.props.navigation.getParam('event').info.timeStart, 1), 
            timeEnd: this.getDates(this.props.navigation.getParam('event').info.timeEnd, 2), 
            description: this.props.navigation.getParam('event').info.description[0].description
        })
        if(this.props.navigation.getParam('event').info.tickets[0].discountedprice == 0) {
            this.setState({cost: 'Free Ticket is available'})
        }
        else {
            this.setState({cost: String('Tickets start from Rs').concat('.', this.props.navigation.getParam('event').info.tickets[0].discountedprice)})
        }
        this.createTicket(this.props.navigation.getParam('event').info.tickets)
    }

    createTicket = (tickets) => {
        console.log(tickets)
        let types = []
        tickets.forEach(ticket => {
            types = [...types, {value: ticket.name, price: ticket.discountedprice}]
        })
        this.setState({ticketType: types})
    }

    generateDataCarousel = () => {
        let counter = 0
        this.props.navigation.getParam('event').info.images.forEach((image) => {
            this.setState({datacarousel: [...this.state.datacarousel, {id: counter++, imagePath: image.address}]})
        })
    }

    getDates(raw, type) {
        let month = new Date(raw).getMonth()
        let date = new Date(raw).getDate()
        let time = new Date(raw).getHours()
        let minutes = new Date(raw).getMonth()
        let formated = ''
        if(minutes < 10) {
            minutes = String('0').concat('', String(minutes))
        }
        if(time > 12) {
            time = time - 12
            if(time < 10) {
                time = String('0').concat('', String(time)).concat(':', String(minutes))
            }
            time = String(time).concat('', 'PM')
        }
        else {
            if(time < 10) {
                time = String('0').concat('', String(time)).concat(':', String(minutes))
            }
            time = String(time).concat('', 'AM')
        }
        switch(month) {
            case(0): {month = 'JAN'; break};
            case(1): {month = 'FEB'; break};
            case(2): {month = 'MAR'; break};
            case(3): {month = 'APR'; break};
            case(4): {month = 'MAY'; break};
            case(5): {month = 'JUN'; break};
            case(6): {month = 'JUL'; break};
            case(7): {month = 'AUG'; break};
            case(8): {month = 'SEP'; break};
            case(9): {month = 'OCT'; break};
            case(10): {month = 'NOV'; break};
            case(11): {month = 'DEC'; break};           
        }
        if(type == 1) {
            formated = String(month).concat(' ', String(date)).concat(', ', time)
        }
        else if(type == 2) {
            formated = String(date).concat(', ', time)
        }
        return formated;
    }

    _renderItem = ({item}) => {
        return (
            <Text style={styles.tag} > {item} </Text>
        )
    }

    _keyExtractor = (item, index) => item

    accordionHandler = () => {
        if(this.state.expansion) {
            this.setState({expansion: false, btn: 'show less'})
        }
        else {
            this.setState({expansion: true, btn: 'show more'})
        }
    }

    setModalVisible = (visible) => {
        this.setState({modalVisible: visible});
    }

    handleChange = (name, value) => {
        this.setState({
            [name]: value,
        });
    };

    render() {
        
        return (
            <View style={styles.container} >
                <ScrollView contentContainerStyle={{flexGrow: 1}} overScrollMode='always' >
                    <View style={styles.carousel} >
                        <Carousel
                            data={this.state.datacarousel}
                            onBeforeSnapToItem={(index) => this.setState({ activeSlide: index }) }
                            sliderWidth={this.state.sliderWidth.width}
                            itemWidth={this.state.sliderWidth.width}
                            renderItem={CarouselItem}
                            hasParallaxImages={true} />
                        
                        <Pagination
                            dotsLength={this.state.datacarousel.length}
                            activeDotIndex={this.state.activeSlide}
                            // tappableDots={true}
                            // carouselRef={CarouselItem}
                            containerStyle={styles.pagination}
                            dotStyle={{
                                width: 6,
                                height: 6,
                                borderRadius: 3,
                                backgroundColor: 'rgba(255, 255, 255, 0.92)'
                            }}
                            inactiveDotStyle={{
                                // Define styles for inactive dots here
                            }}
                            inactiveDotOpacity={0.4}
                            inactiveDotScale={0.4} />
                        <EventHeader 
                            left={this.props.navigation.getParam('closeModal')}
                            leftIcon="chevron-left"
                            leftSize={30}
                            title={this.props.navigation.getParam('event').info.name}
                            right={() => console.log('share')}
                            rightIcon="share"
                           rightSize={30}
                            hide={false} />
                    </View>
    
                    <View style={styles.summary} >
                        <View style={styles.summaryView} >
                            <View style={styles.summaryIcon} >
                                <TouchableOpacity onPress={() => alert('beep')} >
                                    <FontAwesome name="calendar-plus-o" size={24} color="#2196f3" />
                                </TouchableOpacity>                                
                            </View>
                            <Text style={styles.summaryText} >
                                {this.state.timeStart} - {this.state.timeEnd}
                            </Text>
                            </View>
                        <View style={styles.summaryView} >
                            <View style={styles.summaryIcon} >
                                <TouchableOpacity onPress={() => alert('beep')} >
                                    <FontAwesome name="location-arrow" size={24} color="#2196f3" />
                                </TouchableOpacity>                                
                            </View>
                            <Text style={styles.summaryText} >
                                {this.props.navigation.getParam('event').info.venue}
                            </Text>
                            </View>
                        <View style={styles.summaryView} >
                            <View style={styles.summaryIcon} >
                                <TouchableOpacity onPress={() => alert('beep')} >
                                    <FontAwesome name="ticket" size={24} color="#2196f3" />
                                </TouchableOpacity>                                
                            </View>
                            <Text style={styles.summaryText} >
                                {this.state.cost}
                            </Text>
                        </View>
                    </View>
                        
                    <EventDescription
                        accordionHandler={this.accordionHandler}
                        children={this.state.description}
                        expansion={this.state.expansion}
                        btn={this.state.btn} />

                    <Venue 
                        initialRegion={{
                            latitude: this.props.navigation.getParam('event').info.geoLocation.latitude,
                            longitude: this.props.navigation.getParam('event').info.geoLocation.longitude,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                        }}
                        latlng={this.props.navigation.getParam('event').info.geoLocation}
                        title="Beep" />

                    <View style={styles.contactConatiner} >
                        <Text style={styles.contactText} >Contacts</Text>
                        <View style={styles.summaryView} >
                            <View style={styles.summaryIcon} >
                                <TouchableOpacity onPress={() => alert('beep')} >
                                    <MaterialCommunityIcons name='phone-in-talk' size={24} color='#2196f3' />
                                </TouchableOpacity>
                            </View>
                            <Text style={styles.summaryText} >
                                {this.props.navigation.getParam('event').info.contact.phone}
                            </Text>
                        </View>
                        <View style={styles.summaryView} >
                            <View style={styles.summaryIcon} >
                                <TouchableOpacity onPress={() => alert('beep')} >
                                    <Ionicons name='ios-mail' size={24} color='#2196f3' />
                                </TouchableOpacity>
                            </View>
                            <Text style={styles.summaryText} >
                                {this.props.navigation.getParam('event').info.contact.email}
                            </Text>
                        </View>
                    </View>
    
                    <View style={styles.tagsConatiner} >
                        <Text style={styles.tagsText} >Tags</Text>
                        <View style={styles.tags} >
                            <FlatList
                                contentContainerStyle={styles.tags}
                                data={this.props.navigation.getParam('event').info.tags}
                                keyExtractor={this._keyExtractor}
                                renderItem={this._renderItem} />
                        </View>
                    </View>
                </ScrollView>

                <View style={styles.viewTickets} >
                    <Modal 
                        style={styles.modal}
                        isVisible={this.state.modalVisible}
                        animationIn='slideInUp'
                        onBackdropPress={() => this.setModalVisible(false)} >
                        <TicketCounter 
                            type={this.state.type}
                            count={this.state.count}
                            ticketType={this.state.ticketType}
                            handleChange={this.handleChange}
                            code={this.state.code} />
                    </Modal>
                    <Button mode="contained" color="#2E86C1" onPress={() => this.setModalVisible(true)} >
                        <Text style={{fontFamily: 'Karla', fontWeight: 'bold', fontSize: 16}} > View Tickets </Text>
                    </Button>
                </View>
            </View>
        )
    }
}

export default EventDetails;