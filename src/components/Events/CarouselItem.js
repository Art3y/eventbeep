import React from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import { ParallaxImage } from 'react-native-snap-carousel';

const Styles = StyleSheet.create({
    container: {
        flex: 1
    },
    imageContainer: {
        height: '100%',
        width: '100%'
    },  
    image: {
        height: '100%',
        width: '100%'
    }
})

const CarouselItem = ({item, index}, props) => {
    return(
        <View style={Styles.container} >
            <ParallaxImage
                source={{ uri: item.imagePath }}
                containerStyle={Styles.imageContainer}
                style={Styles.image}
                parallaxFactor={0.4}
                resizeMode='contain'
                resizeMethod="resize"
                {...props} />
        </View>
    )
}

export default CarouselItem;