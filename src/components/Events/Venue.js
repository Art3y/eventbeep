import React from 'react';
import { MapView, PROVIDER_GOOGLE } from 'expo';
import { StyleSheet, Dimensions, View, Text } from 'react-native';

const height = Dimensions.get('window').height

const Style = StyleSheet.create({
    wrap: {
        width: '100%',
        height: height * 0.4,
        padding: 16,
        backgroundColor: '#fff',
        marginTop: 10
    },
    map: { 
        flex: 1,
    },
    head: {
        fontSize: 20,
        fontWeight: 'bold'
    }
})

const Venue = (props) => {
    return (
        <View style={Style.wrap} >
            <Text style={Style.head} >Venue</Text> 
            <MapView
                accessible={false}
                provider={PROVIDER_GOOGLE}
                initialRegion={props.initialRegion}
                style={Style.map}>
                <MapView.Marker
                    coordinate={{longitude:props.latlng.longitude, latitude: props.latlng.latitude}}
                    title={props.title} />
            </MapView>
        </View>
    );
}

export default Venue