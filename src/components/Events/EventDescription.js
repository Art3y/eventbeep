import React from 'react';
import {StyleSheet, View, Dimensions, WebView, Text} from 'react-native';
import { Button } from 'react-native-paper';
import MyWebView from 'react-native-webview-autoheight';

const height = Dimensions.get('window').height

var Styles = StyleSheet.create({
    container:{
        flexGrow: 1,
        position: 'relative',
        backgroundColor: '#fff',
        overflow: 'hidden',
        marginTop: 10,
        minHeight: height * 0.4
    },
    button:{
        flexShrink: 1,
        position: 'absolute',
        bottom: 2,
        right: 4
    },
    body:{
        padding     : 10,
        paddingTop  : 0
    },
    children: {
        flex    : 1,
        color   :'#2a2f43',
    }
})

const EventDescription = (props) => {

    return (
        <View style={[Styles.container, {minHeight: height * 0.4}]} >
            <Text style={{fontFamily: 'Karla', fontSize: 18, fontWeight: 'bold'}} >Description</Text>
            {props.expansion ? 
                <MyWebView
                    javaScriptEnabled={true}
                    originWhitelist={['*']} 
                    source={{html:props.children}} /> 
            :
                <WebView
                    style={{flex: 1, width: '100%'}}
                    originWhitelist={['*']} 
                    source={{html:props.children}} />
            }
            <Button style={Styles.button} mode='outlined' color='yellow' onPress={props.accordionHandler} >{props.btn}</Button>
        </View>
    )
}

export default EventDescription;