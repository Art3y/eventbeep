import React from 'react';
import { Image, StyleSheet, Text, View, Dimensions, TouchableOpacity } from 'react-native';
import { Card } from 'react-native-paper';
import { MaterialIcons } from '@expo/vector-icons';

const height = Dimensions.get('window').height

const styles = StyleSheet.create({
    card: {
        flex: 1,
        flexDirection: 'column',
        marginBottom: 12,
        height: height * 0.4,
        borderRadius: 8,  
    },
    image: {
        flex: 14,
        borderTopRightRadius: 8,
        borderTopLeftRadius: 8,
    },
    info: {
        flex: 4,
        paddingTop: 16,
        paddingBottom: 8,
        flexDirection: 'row',
        borderBottomEndRadius: 8,
    },
    date: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        borderBottomLeftRadius: 8,      
    },
    venue: {
        flex: 7,
        paddingStart: 6,
        paddingEnd: 6,
        flexDirection: 'column',
        borderBottomRightRadius: 8,
    },
    venue1: {
        flex: 1,
        paddingTop: 4,
        paddingBottom: 4,
    },
    venue2: {
        flex: 1,
        paddingTop: 4,
        paddingBottom: 4,
        flexDirection: 'row',
        alignItems: 'center',
    }
});

const EventCard = (props) => {

    return (
        <TouchableOpacity onPress={props.openModal} >
            <Card style={styles.card} bordered >
                <Image style={styles.image} source={{uri: props.image}} />
                <View style={styles.info} >
                    <View style={styles.date} >
                        <Text style={{fontFamily: 'Karla'}} > {props.month} </Text>
                        <Text style={{fontFamily: 'Karla', fontSize: 20, fontWeight: '400'}} > {props.date} </Text>
                        <Text style={{fontFamily: 'Karla'}} > {props.year} </Text>
                    </View>
                    <View style={styles.venue} >
                        <View style={styles.venue1} >
                            <Text style={{fontFamily: 'Karla', fontWeight: 'bold', fontSize: 18}} > {props.name} </Text>
                        </View>
                        <View style={styles.venue2} >
                            <MaterialIcons name="location-on" size={16} color="grey" />                
                            <Text style={{fontFamily: 'Karla', color: 'grey'}} > {props.venue} </Text>
                        </View>
                    </View>
                </View>
            </Card>
        </TouchableOpacity>
    )
}

export default EventCard;