import React from 'react';
import { View, StyleSheet, Dimensions, Text } from 'react-native';
import { Button, TextInput } from 'react-native-paper';
import { Dropdown } from 'react-native-material-dropdown';
import Slider from 'react-native-slider';

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

const Style = StyleSheet.create({
    container: {
        height: height * 0.5,
        width: width,
        margin: 0,
        borderTopLeftRadius: 24,
        borderTopRightRadius: 24,
        backgroundColor: '#fff',
        padding: 8
    },
    type: {
        flex: 1,
        marginBottom: 2
    },
    count: {
        flex: 1,
        marginBottom: 2
    },
    coupon: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 2
    },
    submit: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    code: {
        flex: 7,
    },
    apply: {
        flex: 3,
    },
    input: {
        justifyContent: 'center',
        backgroundColor: '#fff'
    },
    btn: {
        marginLeft: 6,
        marginRight: 6,
        width: '40%'
    },
    heading: {
        fontFamily: 'Karla',
        color: 'blue',
        fontSize: 16
    }
})

const TicketCounter = (props) => {
    return(
        <View style={Style.container} >
            <Text style={Style.heading} >Ticket Type</Text>
            <View style={Style.type} >
                <Dropdown 
                    label='Select ticket'
                    value={props.type}
                    data={props.ticketType} />
            </View>
            <Text style={Style.heading} >Ticket Count</Text>            
            <View style={Style.count} >
                <Slider 
                    value={props.count}
                    step={1}
                    minimumValue={0}
                    maximumValue={12}
                    onValueChange={(count) => props.handleChange('count', count)} />
                <Text>The value is: {props.count}</Text>
            </View>        
            <Text style={Style.heading} >Coupon code</Text>
            <View style={Style.coupon} >
                <View style={Style.code} >
                    <TextInput 
                        style={Style.input}
                        label='Coupon code'
                        value={props.code}
                        onChangeText={(text) => props.handleChange('code', text)} />
                </View>
                <View style={Style.apply} >
                    <Button mode="contained" color="#2E86C1" onPress={() => console.log('hi')} >
                        <Text style={{fontFamily: 'Karla', fontWeight: 'bold', fontSize: 16}} > Apply </Text>
                    </Button>    
                </View>
            </View>
            <View style={Style.submit} >
                <Button style={Style.btn} mode="contained" color="#2E86C1" onPress={() => console.log('hi')} >
                    <Text style={{fontFamily: 'Karla', fontWeight: 'bold', fontSize: 16}} > Pay cash </Text>
                </Button>
                <Button style={Style.btn} mode="contained" color="#2E86C1" onPress={() => console.log('hi')} >
                    <Text style={{fontFamily: 'Karla', fontWeight: 'bold', fontSize: 16}} > Pay cash </Text>
                </Button>
            </View>
        </View>
    )
}

export default TicketCounter