import React, { Component } from 'react';
import { AppLoading, Asset } from 'expo';
import { withNavigation } from 'react-navigation';
import { View, StyleSheet} from 'react-native';
import { Tab, Tabs, ScrollableTab } from 'native-base';
import FestivalTab from './FestivalTab';
import HomeHeader from '../Header/HomeHeader';

const Styles = StyleSheet.create({
    wrap: {
        flex: 1,
        backgroundColor: '#e0e0e0',
    },
    tab: {
        
    },
    tabText: {
        fontFamily: 'Karla',
    },
    activeTabText: {
        fontFamily: 'Karla',
    },
    underline: {
        height: 1
    }
})

class FestivalHome extends Component {

    constructor(props) {
		super(props)
		this.state = {
			ready: false,
			loading: false,
			headerLogo: null,
            modalVisible: false,
            modalType: 0,
            tabComponents: []
        }
        console.log(this.props.navigation.getParam('fest').id)
	}

    componentDidMount() {
        this.createTabs(this.props.navigation.getParam('fest').fest.categories)
    }

    createTabs = (tabs) => {
        let counter = 0
        let comp = [
            <Tab key={counter++} textStyle={Styles.tab} activeTextStyle={Styles.activeTabText} tabStyle={Styles.tab} heading='All' >
                <FestivalTab fest={this.props.navigation.getParam('fest').id} eventCategories={'all'} />
            </Tab>
        ]
        tabs.forEach(tab => {
            comp = [...comp, 
            <Tab key={counter++} textStyle={Styles.tab} activeTextStyle={Styles.activeTabText} tabStyle={Styles.tab} heading={tab} >
                <FestivalTab fest={this.props.navigation.getParam('fest').id} eventCategories={tab} />
            </Tab>]
        })
        this.setState({tabComponents: comp})
    }

    render() {
        // if(!this.state.ready) {
        //     return (
        //         <AppLoading
        //             startAsync={this._cacheResourceAsync}
        //             onFinish={() => this.setState({ready: true})}
        //             onError={console.warn} />
        //     )
        // }
        
        return (
            <View style={Styles.wrap} >
                <HomeHeader navigation={this.props.navigation} />
                <View style={{flex: 1}} >
                    <Tabs tabBarUnderlineStyle={Styles.underline} renderTabBar={()=> <ScrollableTab />} >
                        {this.state.tabComponents}
                    </Tabs>
                </View>
            </View>
        );
    };
};

export default withNavigation(FestivalHome);