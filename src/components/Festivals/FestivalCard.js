import React from 'react';
import { Image, StyleSheet, Text, View, Dimensions, TouchableOpacity } from 'react-native';
import { Card } from 'react-native-paper';

const height = Dimensions.get('window').height

const styles = StyleSheet.create({
    card: {
        flex: 1,
        margin: 6,
        flexDirection: 'column',
        marginBottom: 12,
        height: height * 0.4,
        borderRadius: 8,  
    },
    image: {
        flex: 14,
        borderTopRightRadius: 8,
        borderTopLeftRadius: 8,
    },
    content: {
        flex: 4,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

const FestivalCard = (props) => {

    return (
        <TouchableOpacity onPress={props.openModal} >
            <Card style={styles.card} bordered >
                <Image style={styles.image} source={{uri: props.image}} />
                <View style={styles.content} >
                    <Text style={{fontFamily: 'Karla', fontSize: 20, fontWeight: 'bold'}} >{props.name}</Text>
                    <Text style={{fontFamily: 'Karla', color: 'grey'}} >{props.period}</Text>
                </View>
            </Card>
        </TouchableOpacity>
    )
}

export default FestivalCard;