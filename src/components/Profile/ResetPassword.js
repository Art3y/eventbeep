import React from 'react';
import { Dimensions, StyleSheet, View } from 'react-native';
import {  } from 'react-native-paper';
import ModalHeader from '../Header/ModalHeader';

const height = Dimensions.get('window').height

const Styles = StyleSheet.create({
    container: {
        flex: 1
    }
})

const ResetPassword = (props) => {
    
    if(props.disabled)
        return null
    
    return (
        <View style={Styles.container} >
            <ModalHeader
                left={props.closeModal}
                leftIcon="chevron-left"
                leftSize={30}
                title="Reset Password"
                hide={true} />    
        </View>
    )
}

export default ResetPassword;