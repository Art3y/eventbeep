import React from 'react';
import { View, StyleSheet, Image, Text, Dimensions } from 'react-native';

const height = Dimensions.get('window').height

const Styles = StyleSheet.create({
    container: {
        height: height * 0.3,
        width: '100%',
        marginTop: 20,
    },
    content: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 16
    },
    card: {
        width: '40%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    qr: {
        width: '100%',
        aspectRatio: 1
    },
    code: {
        flex: 1,
        fontSize: 20,
        fontWeight: 'bold',
        justifyContent: 'center',
        alignItems: 'center'
    }
})

const Refferal = (props) => {
    
    return(
        <View Styles={Styles.container} >
            <Text style={{paddingLeft: 6}} >Buffer Refferal</Text>
            <View style={Styles.content} >
                <View style={Styles.card} >
                    <Image style={Styles.qr} source={{uri: 'https://upload.wikimedia.org/wikipedia/commons/2/2d/Qr-3.png'}} />
                    <Text style={Styles.code} >UIVWXY</Text>
                </View>
            </View>
        </View>
    )
} 

export default Refferal