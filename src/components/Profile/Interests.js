import React from 'react';
import { View, StyleSheet, Text, Dimensions, FlatList, ScrollView } from 'react-native';
import { IconButton, Colors } from 'react-native-paper';

const height = Dimensions.get('window').height

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        minHeight: height * 0.2,
        width: '100%',
        position: 'relative',
        flexDirection: 'column',
        marginTop: 20,
    },
    heading: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 6
    },
    btn: {
        marginLeft: 'auto',
    },
    label: {
        backgroundColor: 'yellow',
        margin: 8,
        padding: 4
    },
    tags: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    }
})

const Interests = (props) => {
    return(
        <View style={Styles.container} >
            <View style={Styles.heading} >
                <Text>My Interests</Text>
                <IconButton 
                    style={Styles.btn}
                    icon="add"
                    // color={Colors.blueA100}
                    size={20}
                    onPress={props.openModal} />
            </View>
            <ScrollView horizontal style={{flex: 2, flexDirection: 'row'}} showsHorizontalScrollIndicator={false} >
                <FlatList
                    // numColumns={3}
                    contentContainerStyle={Styles.tags}
                    data={props.interests}
                    keyExtractor={props._keyExtractor}
                    renderItem={props._renderItem} />
            </ScrollView>
        </View>
    )
} 

export default Interests