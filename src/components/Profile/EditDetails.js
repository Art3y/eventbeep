import React from 'react';
import { View, StyleSheet, Image, KeyboardAvoidingView, ScrollView, Dimensions, DatePickerIOS, FlatList } from 'react-native';
import { TextInput } from 'react-native-paper';
import ModalHeader from '../Header/ModalHeader';
// import { Picker } from 'native-base';

const height = Dimensions.get('window').height

const Style = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        backgroundColor: '#fff',
    },
    iconContainer: {
        margin: 12,
        justifyContent: 'center',
        alignItems: 'center',
        height: height * 0.16,
        width: height * 0.16,
    },
    icon: {
        justifyContent: 'center',
        alignItems: 'center',
        height: height * 0.16,
        width: height * 0.16,
        borderRadius: height * 0.08,
        backgroundColor: '#ECF0F1',
    },
    iconImage: {
        height: height * 0.12,
        width: height * 0.12,
    },
    InputContainer:{
        flex: 1,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 12
    },
    input: {
        marginBottom: 4,
        width: '100%',
        backgroundColor: '#fff'
    },
    date: {
        width: '100%'
    },
    wrap: {
        width: '100%',
        flexDirection: 'row',
    },
    wrapInput1: {
        marginRight: 1,
        marginBottom: 4,
        width: '50%',
        backgroundColor: '#fff'
    },
    wrapInput2: {
        marginLeft: 1,
        marginBottom: 4,
        width: '50%',
        backgroundColor: '#fff'
    }
});

const EditDetails = (props) => {
    
    if(props.disabled)
        return null

    else {
        return (
            <KeyboardAvoidingView behavior='padding' style={Style.container} >
                <ModalHeader 
                    left={props.closeModal}
                    leftIcon="chevron-left"
                    leftSize={30}
                    title="Edit Profile"
                    right={props.save}
                    rightIcon="check"
                    rightSize={30}
                    hide={false} />
                <ScrollView >
                    {/* <View style={Style.InputContainer} > */}
                    <KeyboardAvoidingView behavior='padding' style={Style.InputContainer} >
                        <View style={Style.iconContainer} >
                            <View style={Style.icon} >
                                <Image resizeMode='contain' style={Style.iconImage} source={props.headerLogo} />
                            </View>
                        </View>
                        <TextInput
                            style={Style.input}
                            label="Full Name"
                            mode='outlined'
                            textContentType='name'
                            keyboardType='name-phone-pad'
                            value={props.inputs.name}
                            onChangeText={(text) => props.handleChange('name', text)} />
                        {/* <TextInput
                            style={Style.input}
                            label="Date of Birth"
                            mode='outlined'
                            textContentType='none'
                            value={props.inputs.name} /> */}
                        {/* <View style={Style.date} >
                            <DatePickerIOS
                                date={props.date}
                                onDateChange={props.setDate} />
                        </View> */}
                        <View style={Style.wrap} >
                            <TextInput
                                style={Style.wrapInput1}
                                label="Branch"
                                mode='outlined'
                                keyboardType='name-phone-pad'
                                value={props.inputs.branch}
                                onChangeText={(text) => props.handleChange('branch', text)} />
                            <TextInput
                                style={Style.wrapInput2}
                                label="Year of passing"
                                mode='outlined'
                                textContentType='postalCode'
                                maxLength={4}
                                keyboardType='number-pad'
                                value={props.inputs.branch}
                                onChangeText={(text) => props.handleChange('branch', text)} />
                        </View>
                        {/* <View style={{width: '100%'}} >
                            <Picker
                                mode='dialog'
                                style={{ height: 50, width: 100 }}
                                selectedValue={props.degree}
                                onValueChange={props.degreeHandler} >
                                <FlatList
                                    data={props.degree}
                                    keyExtractor={props._keyExtractor}
                                    renderItem={props._renderItem} />
                            </Picker>
                        </View> */}
                        <TextInput
                            style={Style.input}
                            label='Degree(Eg. B.Tech, BE)'
                            value={props.inputs.degree}
                            mode='outlined' />
                        <TextInput
                            style={Style.input}
                            label='College Name'
                            disabled
                            value={props.inputs.college}
                            mode='outlined' />
                        <TextInput
                            style={Style.input}
                            label='Phone Number'
                            textContentType='telephoneNumber'                        
                            disabled
                            value={props.inputs.phone}
                            mode='outlined' />
                        <TextInput
                            style={Style.input}
                            mode='outlined'
                            label='Whatsapp Number'
                            textContentType='telephoneNumber'
                            keyboardType='phone-pad'
                            maxLength={10}
                            value={props.inputs.whatsapp}
                            onChangeText={(text) => props.handleChange('whatsapp', text)} />
                        <TextInput
                            style={Style.input}
                            label='Email Address'
                            value={props.inputs.email}
                            onChangeText={(text) => props.handleChange('email', text)}
                            mode='outlined'
                            keyboardType='email-address'
                            textContentType='emailAddress' />
                    </KeyboardAvoidingView>
                    {/* </View> */}
                </ScrollView>
            </KeyboardAvoidingView>
        );    
    }
}

export default EditDetails;