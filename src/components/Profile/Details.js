import React from 'react';
import { View, StyleSheet, Image, Text, Dimensions } from "react-native";
import { IconButton, Colors } from "react-native-paper";

const height = Dimensions.get('window').height

const Styles = StyleSheet.create({
    wrap: {
        position: 'relative',
        marginTop: 20,
        height: height * 0.6,
        width: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    icon: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: 0,
        height: height * 0.2,
        aspectRatio: 1,
        borderRadius: height * 0.1,
        backgroundColor: '#ffffff',
    },
    iconImage: {
        height: 60,
        width: 60
    },
    card: {
        backgroundColor: '#ffffff',
        position: 'relative',
        height: height * 0.5,
        width: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30,
    },
    details: {
        flex: 1,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    heading: {
        flex: 1,
        color: 'blue'
    },
    content: {
        flex: 1,
    },
    borderLine: {
        width: '90%',
        height: 2,
        borderBottomColor: '#ECF0F1',
        borderBottomWidth: 1
    },
    openModal: {
        marginLeft: 'auto',
    }
})

const Details = (props) => {

    return (
        <View style={Styles.wrap}>
            <View style={{height: height * 0.1, width: '100%'}}></View>
            <View style={Styles.card} >
                <View style={{flex: 1, width: '100%'}} >
                    <IconButton
                        style={Styles.openModal} 
                        icon="edit"
                        // color={Colors.red500}
                        size={20}
                        onPress={props.openModal} />
                </View>
                <View style={Styles.details} >
                    <Text style={Styles.heading} >Name</Text>
                    <Text style={Styles.content} > {props.name} </Text>
                    <View style={Styles.borderLine} />
                </View>
                <View style={Styles.details} >
                    <Text style={Styles.heading} >Email</Text>
                    <Text style={Styles.content} > {props.email} </Text>
                    <View style={Styles.borderLine} />
                </View>
                <View style={Styles.details} >
                    <Text style={Styles.heading} >Mobile Number</Text>
                    <Text style={Styles.content} > {props.phone} </Text>
                    <View style={Styles.borderLine} />
                </View>
                <View style={Styles.details} >
                    <Text style={Styles.heading} >College Name</Text>
                    <Text style={Styles.content} > {props.college} </Text>
                </View>
            </View>
            <View style={Styles.icon} >
                <Image style={Styles.iconImage} source={props.headerLogo} />
            </View>
        </View>
    )
}

export default Details;