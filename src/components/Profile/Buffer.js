import React from 'react';
import { View, StyleSheet, Text, Dimensions } from 'react-native';
import { Surface, Button } from 'react-native-paper';

const height = Dimensions.get('window').height

const Styles = StyleSheet.create({
    container: {
        width: '100%',
        height: height * 0.3,
        marginTop: 20
    },
    card: {
        flex: 1,
        marginTop: 16,
        alignItems: 'center',
        justifyContent: 'center',
    },
    btn: {
        fontSize: 14,
        fontWeight: 'bold',
        color: 'black'
    }
})

const Buffer = (props) => {
    return(
        <View style={Styles.container} >
            <Text style={{paddingLeft: 6}} >Added Buffer</Text>
            <Surface style={Styles.card} >
                <Button  mode='text' onPress={props.openModal} > 
                    <Text style={Styles.btn} > Reset your Password </Text>
                </Button>
                <Button  mode='text' onPress={() => console.log('hi')} > 
                    <Text style={Styles.btn} > Share with peeps </Text>
                </Button>
                <Button  mode='text' onPress={() => console.log('hi')} > 
                    <Text style={Styles.btn} > Get Help </Text>
                </Button>
                <Button  mode='text' onPress={() => console.log('hi')} > 
                    <Text style={Styles.btn} > Feedback </Text>
                </Button>
            </Surface>
        </View>
    )
} 

export default Buffer