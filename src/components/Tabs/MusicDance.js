import React, { Component } from 'react';
import { AppLoading, Asset } from 'expo';
import { View, StyleSheet, Image, Text, Dimensions, FlatList } from 'react-native';
import EventCard from '../Events/EventCard';
import firebase from 'firebase';
require('firebase/firestore');

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

const Styles = StyleSheet.create({
    wrap: {
        flex: 1,
        backgroundColor: '#f5f5f5',
        paddingTop: 6,
        paddingLeft: 8,
        paddingRight: 8,
    },
    loadingWrap: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f5f5f5'
    },
    loadingImage: {
        height: height * 0.5,
        width: width * 0.7
    },
    loadingText: {
        marginTop: 10,
        fontFamily: 'Helvetica',
        fontSize: 24,
        fontWeight: '600',
        color: '#757575'
    }
})

class MusicDance extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            events: [],
            lastEvent: null,
            expandedEvent: 0,
            loading: true
        }
        this.fetchEvents()
    }

    async _cacheResourceAsync() {
        const images = [
            require('../../Assets/Images/no_events.png')
        ];
		
        const cacheImages = images.map((image) => {
            return Asset.fromModule(image).downloadAsync();
        });
        
        return Promise.all(cacheImages)
    }

    fetchEvents = () => {
        firebase.firestore().collection('events-s').where('categories', '==', 'music and dance').where('expired', '==', false).get()
            .then((snapshot) => {
                if(snapshot.empty) {
                    this.setState({loading: true})
                    return
                }
                snapshot.forEach((doc) => {
                    this.setState({events: [...this.state.events, {id: doc.id, info: doc.data(), card: this.generateCardData(doc.data())}]})
                })
                var lastEvent = snapshot.docs[snapshot.docs.length - 1].data()
                this.setState({lastEvent: lastEvent})
                this.setState({loading: false})
            })
            .catch((err) => {
                console.log('Error in MusicDance', err)
            })
    }

    generateCardData = (doc) => {
        let cardData = {}
        cardData.imagePrimary = doc.imagePrimary
        if(doc.venue.length > 44) {
            cardData.venue = doc.venue.slice(0, 44).concat("", "...")
        }
        else {
            cardData.venue = doc.venue
        }
        if(doc.name.length > 36) {
            cardData.name = doc.name.slice(0, 36).concat("", "...")
        }
        else {
            cardData.name = doc.name
        }
        date = new Date(doc.timeStart)
        cardData.date = date.getDate()
        cardData.year = date.getFullYear()
        switch(date.getMonth()) {
            case(0): {cardData.month = 'JAN'; break};
            case(1): {cardData.month = 'FEB'; break};
            case(2): {cardData.month = 'MAR'; break};
            case(3): {cardData.month = 'APR'; break};
            case(4): {cardData.month = 'MAY'; break};
            case(5): {cardData.month = 'JUN'; break};
            case(6): {cardData.month = 'JUL'; break};
            case(7): {cardData.month = 'AUG'; break};
            case(8): {cardData.month = 'SEP'; break};
            case(9): {cardData.month = 'OCT'; break};
            case(10): {cardData.month = 'NOV'; break};
            case(11): {cardData.month = 'DEC'; break};           
        }
        return cardData
    }

    fetchMoreEvents = () => {
        // firebase.firestore().collection('events-s').where('categories', '==', 'adventure').startAfter(this.state.lastEvent).limit(5).get()
        //     .then((snapshot) => {
        //         snapshot.forEach((doc) => {
        //             // console.log(doc.id)
        //             this.setState({events: [...this.state.events, {id: doc.id, info: doc.data()}]})
        //         })
        //         this.setState({lastEvent: snapshot.docs[snapshot.docs.length - 1]})
        //     })
        //     .catch((err) => {
        //         console.log('Error is here', err)
        //     })
    }

    openModalHandler = (eventID) => {
        this.setState({expandedEvent: this.state.events.findIndex(item => item.id == eventID)})
        this.props.navigation.navigate('Modal', {event: this.state.events[this.state.expandedEvent], closeModal: () => this.props.navigation.goBack()})
    }

    closeModalHandler = () => {
    }

    _renderItem = ({item}) => {
        return (
            <EventCard
                name={item.card.name}
                venue={item.card.venue}
                date={item.card.date}
                month={item.card.month}
                year={item.card.year}
                image={item.card.imagePrimary}
                openModal={() => this.openModalHandler(item.id)} />
        )
    }

    _keyExtractor = (item, index) => item.id;

    render() {

        if(this.state.loading) {
            return (
                <View style={Styles.loadingWrap} >
                    <AppLoading
						startAsync={this._cacheResourceAsync}
						onFinish={() => this.setState({ready: true})}
						onError={console.warn} />
                    <Image style={Styles.loadingImage} resizeMethod='auto' source={require('../../Assets/Images/no_events.png')} />
                    <Text style={Styles.loadingText} > Sorry! No events here</Text>
                </View>
            )
        }

        return(
            <View style={Styles.wrap} >
                <FlatList
                    data={this.state.events}
                    keyExtractor={this._keyExtractor}
                    renderItem={this._renderItem}
                    onEndReachedThreshold={0.5}
                    onEndReached={this.fetchMoreEvents} />
		    </View>
        );
    }
}

export default MusicDance;
