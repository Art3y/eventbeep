import React, { Component } from 'react';
import { AppLoading, Asset } from 'expo';
import { View, ScrollView, StyleSheet, Image, FlatList, Dimensions, TouchableOpacity, Text } from 'react-native';

const height = Dimensions.get('window').height

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f5f5f5'
    },
    banner: {
        height: height * 0.25,
        width: '100%',
    },
    bannerImage: {
        height: '100%',
        width: '100%'
    },
    wrap: {
        marginTop: 8,
        marginBottom: 8,
        backgroundColor: '#fff',
        width: '100%',
        padding: 8,
    },
    category: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 0.5,
        margin: 4,
        padding: 26,
        height: height * 0.24,
        aspectRatio: 1
    },
    itemImage: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
})

class HomeTab extends Component {

    constructor(props) {
        super(props)
        this.state = {
            cover: null,
            loading: true,
            categories: null,
            ready: false
        }
        this._cacheResourceAsync = this._cacheResourceAsync.bind(this)
        this._keyExtractor = this._keyExtractor.bind(this)
        this.renderItem = this.renderItem.bind(this)
    }

    async _cacheResourceAsync() {
        const images = [
            require('../../Assets/Cover.png'),
            require('../../Assets/categories/Adventure.png'),
            require('../../Assets/categories/CollegeEvents.png'),
            require('../../Assets/categories/Food.png'),
            require('../../Assets/categories/Gaming.png'),
            require('../../Assets/categories/MusicAndDance.png'),
            require('../../Assets/categories/Nightlife.png'),
            require('../../Assets/categories/Sports.png'),
            require('../../Assets/categories/WorkshopsAndSeminars.png'),
        ];
      
        const cacheImages = images.map((image) => {
            return Asset.fromModule(image).downloadAsync();
        });
        
        return Promise.all(cacheImages)
    }

    componentDidMount() {
        const images = [
            {key: require('../../Assets/categories/Adventure.png'), id: 1120, title: 'Adventure', route: 1},
            {key: require('../../Assets/categories/CollegeEvents.png'), id: 2120, title: 'College Events', route: 2},
            {key: require('../../Assets/categories/Food.png'), id: 3365, title: 'Food', route: 3},
            {key: require('../../Assets/categories/Gaming.png'), id: 4465, title: 'Gaming', route: 4},
            {key: require('../../Assets/categories/MusicAndDance.png'), id: 5564, title: 'Music and Dance', route: 5},
            {key: require('../../Assets/categories/Nightlife.png'), id: 6555, title: 'Nightlife', route: 6},
            {key: require('../../Assets/categories/Sports.png'), id: 7582, title: 'Sports', route: 7},
            {key: require('../../Assets/categories/WorkshopsAndSeminars.png'), id: 8892, title: 'Workshops', route: 8},
        ]
        this.setState({categories: images})
        this.setState({cover: require('../../Assets/Cover.png')})
    }

    renderItem = ({item}) => {
        return(
            <TouchableOpacity style={Styles.category} key={item.id} onPress={() => this.props.pageHandler(item.route)} >
                <View style={{ height: '100%', width: '100%'}} >
                    <Image resizeMethod="auto" resizeMode="contain" style={Styles.itemImage} source={item.key} />
                </View>
                <Text style={{fontWeight: '300', paddingTop: 8, color: 'grey', fontFamily: 'Karla'}} > {item.title} </Text>
            </TouchableOpacity>
        )
    }

    _keyExtractor = (item) => item.id;

    render() {

        if(!this.state.ready) {
            return (
                <AppLoading
                    startAsync={this._cacheResourceAsync}
                    onFinish={() => this.setState({ready: true})}
                    onError={console.warn} />
            )
        }

        return(
            <ScrollView style={Styles.container} >
                <View style={Styles.banner} >
                    <Image
                        style={Styles.bannerImage} 
                        resizeMethod="auto"
                        resizeMode="stretch"
                        source={this.state.cover} />
                </View>
                <View style={Styles.wrap} >
                    <Text style={{fontWeight: 'bold', fontSize: 20, fontFamily: 'Karla'}} >Categories</Text>
                    <FlatList
                        data={this.state.categories}
                        keyExtractor={this._keyExtractor}
                        renderItem={this.renderItem}
                        numColumns={2} />
                </View>
		    </ScrollView>
        );
    }
}

export default HomeTab;