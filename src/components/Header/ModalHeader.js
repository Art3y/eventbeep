import React from 'react';
import { StyleSheet, View, Dimensions, Text } from 'react-native';
import { IconButton, Colors } from 'react-native-paper';

const height = Dimensions.get('window').height

const Styles = StyleSheet.create({
    container: {
        height: height * 0.1,
        width: '100%',
        flexDirection: 'row',
    },
    btn: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    head: {
        flex: 3,
        justifyContent: 'center',
        alignItems: 'center'
    },
    content: {
        fontSize: 20,
        fontWeight: 'bold'
    }
})

const ModalHeader = (props) => {

    return(
        <View style={Styles.container} >
            <View style={Styles.btn} >
                <IconButton 
                    icon={props.leftIcon}
                    size={props.leftSize}
                    color={Colors.blue500}
                    onPress={props.left} />
            </View>
            <View style={Styles.head} >
                <Text style={Styles.content} > {props.title} </Text>
            </View>
            <View style={Styles.btn} >
                {props.hide? null: (
                    <IconButton
                    icon={props.rightIcon}
                    size={props.rightSize}
                    color={Colors.blue500}
                    onPress={props.right} />
                )}
            </View>
        </View>
    )
}

export default ModalHeader;