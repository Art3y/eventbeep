import React from 'react';
import { View, Text, Dimensions } from 'react-native';
import ModalHeader from './ModalHeader';

const height = Dimensions.get('window').height

class Search extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        if(this.props.disabled)
            return null

        return (
            <View style={{flex: 1, backgroundColor: '#ffffff'}} >
                <ModalHeader 
                    left={this.props.closeModal}
                    leftIcon="chevron-left"
                    leftSize={30}
                    title="Edit Profile"
                    hide={true} />
                <Text>Search here</Text>
            </View>
        )
    }
}

export default Search;