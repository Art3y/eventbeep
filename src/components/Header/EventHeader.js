import React from 'react';
import { LinearGradient } from 'expo';
import { StyleSheet, View, Dimensions, Text } from 'react-native';
import { IconButton, Colors } from 'react-native-paper';

const height = Dimensions.get('window').height

const Styles = StyleSheet.create({
    container: {
        height: height * 0.1,
        width: '100%',
        flexDirection: 'row',
        position: 'absolute',
        top: 0,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent', 
    },
    btn: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    head: {
        flex: 3,
        justifyContent: 'center',
        alignItems: 'center'
    },
    content: {
        fontSize: 20,
        fontWeight: 'bold',
        fontFamily: 'Karla'
    }
})

const EventHeader = (props) => {

    return(
        <LinearGradient colors={['#fff', 'transparent']} style={Styles.container} >
            <View style={Styles.btn} >
                <IconButton 
                    icon={props.leftIcon}
                    size={props.leftSize}
                    color={Colors.black}
                    onPress={props.left} />
            </View>
            <View style={Styles.head} >
                <Text style={Styles.content} > {props.title} </Text>
            </View>
            <View style={Styles.btn} >
                {props.hide? null: (
                    <IconButton
                    icon={props.rightIcon}
                    size={props.rightSize}
                    color={Colors.black}
                    onPress={props.right} />
                )}
            </View>
        </LinearGradient>
    )
}

export default EventHeader;