import React from 'react';
import { View, StyleSheet, Image, Dimensions } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

const height = Dimensions.get('window').height

const Styles = StyleSheet.create({
    wrap: {
        height: height * 0.1,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#ffffff'
    },
    headerButtonLeft: {
        flex: 1,
        marginStart: 6,
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerButtonRight: {
        flex: 1,
        marginEnd: 6,
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerTitle: {
        flex: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerImage: {
        width: '40%',
        resizeMode: 'contain'
    }
})

const HomeHeader = (props) => {

    return (
        <View style={Styles.wrap} >
            <View style={Styles.headerButtonLeft} >
                <FontAwesome name="search" size={28} color="#2196f3" />
            </View>
            <View style={Styles.headerTitle} >
                <Image style={Styles.headerImage} source={props.headerLogo} />
            </View>
            <View style={Styles.headerButtonRight} >
                <FontAwesome name="shopping-cart" size={28} color="#2196f3" />
            </View>
        </View>
    )
}

export default HomeHeader