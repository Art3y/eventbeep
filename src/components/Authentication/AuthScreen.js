import qs from 'qs';
import Axios from 'axios';
import React from 'react';
import { AppLoading, Asset } from 'expo';
import { View, StyleSheet, Image, Text, TouchableOpacity, Linking, KeyboardAvoidingView } from 'react-native';
import { Button, TextInput } from 'react-native-paper';
import * as firebase from 'firebase';
require('firebase/firestore')

const Style = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
    },
    logoContainer:{
        flex: 7,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    InputContainer:{
        flex: 13,
        width: '100%',
    },
    input: {
        margin: 12,
        backgroundColor: '#fff'
    },
    btn: {
        margin: 12,
        marginTop: 0,
        padding: 6
    },
    btnText: {
        fontFamily: 'Karla',
        fontSize: 24,
        fontWeight: 'bold',
        color: '#fff',
        backgroundColor: '#fff'
    },
    forgot: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 5
    },
    forgotText: {
        fontFamily: 'Karla',
        fontSize: 18,
        fontWeight: 'bold',
        // color: ''
    },
    terms1: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    terms2: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    termsText: {
        fontFamily: 'Karla',
        fontSize: 12,
        color: 'grey'
    },
    linksText: {
        fontFamily: 'Karla',
        fontSize: 12,
        color: 'blue'
    }
});

class AuthScreen extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            phoneNumber:'',
            loading: true,
            isAuthenticationReady: false,
            isAuthenticated: false,
        }
    }

    async _cacheResourceAsync() {
        const images = [
			require('../../Assets/Images/logo.png'),
            require('../../Assets/Images/no_events.png')
        ];
		
        const cacheImages = images.map((image) => {
            return Asset.fromModule(image).downloadAsync();
        });
        
        return Promise.all(cacheImages)
    }

    componentDidMount() {
        this.setState({loading: false})
    }

    openURLHandler = (url) => {
        Linking.canOpenURL(url).then(supported => {
            if(supported) {
                Linking.openURL(url);
            }
            else {
                console.log('Don\'t know how to open URI: ' + this.props.url);
            }
        }) 
    }

    onSubmission = () => {
        
        const otpData = {
            url: 'https://eventbuffer.herokuapp.com/sms',
            data: qs.stringify({mobile: 8380825446}),
            header: {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
        }

        const userMail = String(this.state.phoneNumber).concat('@eventbeep.com')
        firebase.firestore().collection('users').doc(userMail).get()
            .then((doc) => {
                if(!doc.exists) {
                    this.props.navigation.dispatch(SignUpRoute)
                    Axios.post(otpData.url, otpData.data, otpData.header)  
                        .then(() => {
                            this.props.navigation.navigate('SignUp', {phone: this.state.phoneNumber})
                        })
                        .catch()
                }
                else
                    this.props.navigation.navigate('Login', {phone: this.state.phoneNumber})
            })
            .catch(() => {
                console.log('Unable to validate userf from firebase')
            })
    }

    render() {

        if(!this.state.ready) {
            return (
				<View style={Style.container} >
					<AppLoading
						startAsync={this._cacheResourceAsync}
						onFinish={() => this.setState({ready: true})}
						onError={console.warn} />
					<Image resizeMethod='scale' resizeMode='contain' source={require('../../Assets/Images/no_events.png')} />
				</View>
            )
        }

        else {
            return (
                <KeyboardAvoidingView behavior='padding' style={Style.container}>
                    <View style={Style.logoContainer} >
                        <Image 
                            resizeMode="contain" 
                            resizeMethod="scale"
                            source={require('../../Assets/Images/logo.png')} />
                    </View>
                    <View style={Style.InputContainer} >
                        <TextInput
                            style={Style.input}
                            label="Mobile Number"
                            mode='outlined'
                            textContentType='telephoneNumber'
                            keyboardType='phone-pad'
                            maxLength={10}
                            value={this.state.phoneNumber}
                            onChangeText={value => this.setState({ phoneNumber: value })} />
                        <Button style={Style.btn} mode="contained" onPress={this.onSubmission} >
                            <Text style={Style.forgotText} >GET IN</Text>
                        </Button>
                        <TouchableOpacity style={Style.forgot} onPress={() => console.log('forgot password')} >
                            <Text style={Style.forgotText} > Forgot Password ? </Text>
                        </TouchableOpacity>
                        <View style={Style.terms1} >
                            <Text style={Style.termsText} >By logging in you agree to the </Text>
                        </View>
                        <View style={Style.terms2} >
                            <Text style={Style.linksText} onPress={() => this.openURLHandler('https://www.youtube.com/')} >Terms<Text style={Style.termsText} > and <Text style={Style.linksText} onPress={() => this.openURLHandler('https://www.youtube.com/')} >Privacy Policy<Text style={Style.termsText} > of Eventbeep</Text></Text></Text></Text>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            );
        }
    }
}

export default AuthScreen;