import React, { Component } from 'react';
import { View, StyleSheet, Alert, KeyboardAvoidingView, Text } from 'react-native';
import { Button, TextInput } from 'react-native-paper';
import * as firebase from 'firebase';
import CodeInput from 'react-native-confirmation-code-input';
import qs from 'qs';
import Axios from 'axios';
require('firebase/firestore')

const Style = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
        padding: 12,
        paddingTop: 6,
        paddingBottom: 0
    },
    otp: {
        height: 120,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 60,
        paddingBottom: 10,
    },
    input: {
        width: '100%',
        margin: 12,
        backgroundColor: '#fff'
    },
    btn: {
        margin: 12,
        marginTop: 0,
        padding: 6
    },
    btnText: {
        fontFamily: 'Karla',
        fontSize: 24,
        fontWeight: 'bold',
        color: '#fff',
        backgroundColor: '#fff'
    },
    terms1: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    terms2: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    termsText: {
        fontFamily: 'Karla',
        fontSize: 12,
        color: 'grey'
    },
    linksText: {
        fontFamily: 'Karla',
        fontSize: 12,
        color: 'blue'
    }
});

class SignUp extends Component {

    constructor(props) {
        super(props);
        this.state = {
            password: '',
            fullName: '',
            email: '',
            otp: ''
        }
    }

    signupHandler = () => {
        var phone = this.props.navigation.getParam('phone')
        const userMail = String(phone).concat('@eventbeep.com')
        const userData = {
            info:{
                phone: Number(phone),
                email: this.state.email,
                name: this.state.fullName,
                password: String(phone)
            },
            docId: userMail
        }
        
        const otpData = {
            url: 'https://eventbuffer.herokuapp.com/smsverify',
            data: qs.stringify({mobile: Number(8380825446), otp: Number(this.state.otp)}),
            header: {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
        }

        Axios.post(otpData.url, otpData.data, otpData.header)
            .then((response) => {
                if(response.data) {
                    firebase.auth().createUserWithEmailAndPassword(userMail,this.state.password)
                        .then((res) => {
                            firebase.firestore().collection('users').doc(userMail).set(userData.info)
                                .then((snap) => {
                                    console.log(snap)
                                })
                                .catch((err) => {
                                    Alert.alert(err)
                                })
                        })
                        .catch((err) => {
                            Alert.alert(err.message)
                        })
                }
                else {
                    Alert.alert('OTP is wrong', this.state.otp)
                }
            })
            .catch((err) => {
                console.log('OTP verification err', err)
            })
    }

    render() {
        return(
            <KeyboardAvoidingView behavior='padding' style={Style.container} >
                <View style={Style.otp} >
                    <CodeInput
                        className='border-box'
                        space={5}
                        size={30}
                        codeLength={4}
                        autoFocus={false}
                        value={this.state.otp}
                        keyboardType='phone-pad'
                        inputPosition='left'
                        activeColor='blue'
                        inactiveColor='black'
                        codeInputStyle={{ borderWidth: 2 }}
                        onFulfill={(code) => this.setState({otp: code})} />
                </View>
                <TextInput
                    mode="outlined"
                    label="Mobile Number"
                    disabled
                    style={Style.input}
                    value={this.props.navigation.getParam('phone')} />
                <TextInput
                    label="Password"
                    mode="outlined"
                    style={Style.input}
                    secureTextEntry={true}
                    value={this.state.phoneNumber}
                    onChangeText={value => this.setState({ password: value })} />
                <TextInput 
                    label="Full Name"                        
                    mode="outlined"
                    style={Style.input}
                    keyboardType="name-phone-pad"
                    value={this.state.phoneNumber}
                    onChangeText={value => this.setState({ fullName: value })} />
                <TextInput 
                    label="Email"
                    mode="outlined"
                    style={Style.input}
                    keyboardType="email-address"
                    value={this.state.phoneNumber}
                    onChangeText={value => this.setState({ email: value })} />
                <Button style={Style.btn} mode="contained" onPress={this.signupHandler} >
                    <Text style={Style.forgotText} >SIGN UP</Text>
                </Button>
                <View style={Style.terms1} >
                    <Text style={Style.termsText} >By logging in you agree to the </Text>
                </View>
                <View style={Style.terms2} >
                    <Text style={Style.linksText} onPress={() => this.openURLHandler('https://www.youtube.com/')} >Terms<Text style={Style.termsText} > and <Text style={Style.linksText} onPress={() => this.openURLHandler('https://www.youtube.com/')} >Privacy Policy<Text style={Style.termsText} > of Eventbeep</Text></Text></Text></Text>
                </View>
            </KeyboardAvoidingView>
        )
    }
}

export default SignUp;