import React from 'react'
import { createMaterialTopTabNavigator, TabRouter, createNavigationContainer, createNavigator } from 'react-navigation'
import { Text, View, SafeAreaView, TouchableOpacity, StyleSheet } from 'react-native'

import Home from '../components/Tabs/Home';
import AdventuresTab from '../components/Tabs/Adventures';
import CollegeEventsTab from '../components/Tabs/CollegeEvents';
import FoodTab from '../components/Tabs/Food';
import GamingTab from '../components/Tabs/Gaming';
import MusicDanceTab from '../components/Tabs/MusicDance';
import NightlifeTab from '../components/Tabs/Nightlife';
import SportsTab from '../components/Tabs/Sports';
import WorkSemiTab from '../components/Tabs/WorkSemi';

const TabLabelComponent = (props) => {
    
    return (
        <View style={{flex: 1}} >
            <Text style={{fontFamily: 'Karla', color: props.color}} >{props.name}</Text>
        </View>
    )
}

const getTabLabel = (navigation, focused, tintColor) => {
    const{ routeName } = navigation.state
    let name = 'Home'
    switch(routeName) {
        case('Tab1'): {
            name = 'Home'
        }
        break;

        case('Tab2'): {
            name = 'Adventures'
        }
        break;

        case('Tab3'): {
            name = 'College events'
        }
        break;

        case('Tab4'): {
            name = 'Food'
        }
        break;

        case('Tab5'): {
            name = 'Gaming'
        }
        break;
        
        case('Tab6'): {
            name = 'Music and Dance'
        }
        break;

        case('Tab7'): {
            name = 'Nightlife'
        }
        break;

        case('Tab8'): {
            name = 'Sports'
        }
        break;

        case('Tab9'): {
            name = 'Workshops'
        }
        break;
    }
    return <TabLabelComponent name={name} color={tintColor} />
}

const styles = StyleSheet.create({
    tabContainer: {
      flexDirection: 'row',
      height: 48,
    },
    tab: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      margin: 4,
      borderWidth: 1,
      borderColor: '#ddd',
      borderRadius: 4,
    },
});
  
const CustomTabBar = ({ navigation }) => {
    const { routes } = navigation.state;
    return (
      <SafeAreaView style={styles.tabContainer}>
        {routes.map(route => (
          <TouchableOpacity
            onPress={() => navigation.navigate(route.routeName)}
            style={styles.tab}
            key={route.routeName}
          >
            <Text>{route.routeName}</Text>
          </TouchableOpacity>
        ))}
      </SafeAreaView>
    );
};

const CustomTabView = ({ descriptors, navigation }) => {
    const { routes, index } = navigation.state;
    const descriptor = descriptors[routes[index].key];
    const ActiveScreen = descriptor.getComponent();
    return (
      <SafeAreaView forceInset={{ top: 'always' }}>
        <CustomTabBar navigation={navigation} />
        <ActiveScreen navigation={descriptor.navigation} />
      </SafeAreaView>
    );
  };

const CustomTabRouter = TabRouter({
    Tab1: { screen: Home, path: 'Tab1' },
    Tab2: { screen: AdventuresTab, path: 'Tab2'},
    Tab3: { screen: CollegeEventsTab, path: 'Tab3'},
    Tab4: { screen: FoodTab, path: 'Tab4'},
    Tab5: { screen: GamingTab, path: 'Tab5'},
    Tab6: { screen: MusicDanceTab, path: 'Tab6'},
    Tab7: { screen: NightlifeTab, path: 'Tab7'},
    Tab8: { screen: SportsTab, path: 'Tab8'},
    Tab9: { screen: WorkSemiTab, path: 'Tab9'},
})

const CustomTabs = createNavigationContainer(
    createNavigator(CustomTabView, CustomTabRouter, {})
);

// const HomeScreenTabs = createMaterialTopTabNavigator({
//     Tab1: { screen: Home},
//     Tab2: { screen: AdventuresTab},
//     Tab3: { screen: CollegeEventsTab},
//     Tab4: { screen: FoodTab},
//     Tab5: { screen: GamingTab},
//     Tab6: { screen: MusicDanceTab},
//     Tab7: { screen: NightlifeTab},
//     Tab8: { screen: SportsTab},
//     Tab9: { screen: WorkSemiTab},
// }, {
//     optimizationsEnabled: true,
//     defaultNavigationOptions: ({navigation}) => ({
//         tabBarLabel: ({ focused: boolean, tintColor: string }) => getTabLabel(navigation, focused, tintColor)
//     }),
//     animationEnabled: true,
//     // labeled: true,
//     tabBarOptions: {
//         scrollEnabled: true,
//         // tabStyle: {
//         //     display: 'flex',
//         //     flexShrink: 1,
//         // },
//         // style: {
//         //     flex: 0.08,
//         //     backgroundColor: '#ffffff',
//         //     justifyContent: 'center',
//         //     alignItems: 'center'
//         // },
//         // labelStyle: {

//         // },
//         indicatorStyle: {
//             backgroundColor: 'blue',
//         },
//         activeTintColor: 'blue',
//         inactiveTintColor: 'black',
//         // allowFontScaling: true
//     },
// })

export default CustomTabs