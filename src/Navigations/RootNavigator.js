import React from 'react'
import { createBottomTabNavigator, createStackNavigator } from 'react-navigation'
import { Octicons, FontAwesome, MaterialCommunityIcons } from '@expo/vector-icons';
import { Dimensions } from 'react-native';

import FestivalScreen from '../screens/FestivalScreen';
import TimelineScreen from '../screens/TimelineScreen';
import ProfileScreen from '../screens/ProfileScreen';
import HomeScreen from '../screens/HomeScreen';
import EventDetails from '../components/Events/EventDetails';
import FestivalHome from '../components/Festivals/FestivalHome';

const height = Dimensions.get('window').height

// const FestivalContainer = createStackNavigator({
//     MainFestival: {screen: FestivalScreen},
//     ModalFestival: {screen: FestivalHome},
// },{
//     initialRouteName: 'Main',
//     headerMode: 'none',
//     mode: 'modal'
// })

const RootNavigator = createBottomTabNavigator({
    Home: {screen: HomeScreen, navigationOptions: {
            tabBarIcon: ({ focused, horizontal, tintColor }) => (
                <FontAwesome name="home" size={32} color={tintColor} />
            ),
        },
    },
    Festival: {screen: FestivalScreen, navigationOptions: {
            tabBarIcon: ({ focused, horizontal, tintColor }) => (
                <FontAwesome name="line-chart" size={32} color={tintColor} />
            )
        },
    },
    Timeline: {screen: TimelineScreen, navigationOptions: {
            tabBarIcon: ({ focused, horizontal, tintColor }) => (
                <MaterialCommunityIcons name="chart-bubble" size={32} color={tintColor} />
            )
        },
    },
    Profile: {screen: ProfileScreen, navigationOptions: {
            tabBarIcon: ({ focused, horizontal, tintColor }) => (
                <Octicons name="person" size={32} color={tintColor} />
            )
        },
    },
}, {
    initialRouteName: 'Festival',
    tabBarOptions: {
        activeTintColor: '#2196f3',
        inactiveTintColor: 'black',
        showLabel: false,
        style: {
            height: height * 0.08
        }
    },
});

const Root = createStackNavigator({
    Main: {
        screen: RootNavigator
    },
    Modal: {
        screen: EventDetails
    },
    FestivalHome: {
        screen: FestivalHome
    }
}, {
    initialRouteName: 'Main',
    headerMode: 'none',
    mode: 'modal'
})

export default Root;