import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import RootNavigator from './RootNavigator';
import AuthScreen from '../components/Authentication/AuthScreen';
import SignUpScreen from '../components/Authentication/SignUpScreen';
import LoginScreen from '../components/Authentication/LoginScreen';

const AppNavigation = createSwitchNavigator(
    {
        AuthLoading: {screen: AuthScreen},
        Login: {screen: LoginScreen},
        SignUp: {screen: SignUpScreen},
        Root: {screen: RootNavigator},
    },
    {
        initialRouteName: 'AuthLoading',
        mode: 'modal',
        navigationOptions: ({navigation, screenProps}) => ({
            header: null,
            headerMode: 'none',
            tabBarVisible: navigation.state.index === 3        
        })
    }
);

// const AppContainer = createAppContainer(AppNavigation)
// const AppContainer = AppNavigation

export default AppNavigation